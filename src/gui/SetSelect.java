package gui;

import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.ListSelectionModel;
//import logic.Sets;
import javax.swing.JButton;

public class SetSelect extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6718624109369067319L;
	private static JList<String> list;
	private static ArrayList<String> setNames=getSetNames();
	private static ArrayList<String> setList=readSetList();
	private static DefaultListModel<String> listModel = new DefaultListModel<String>();
	private ButtonEventHandler eventHandler = new ButtonEventHandler();
	private JPanel panel;
	private Game game = new Game();
	public SetSelect() {
		setBounds(0, 0, 800, 600);
		setLayout(null);
		setVisible(true);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 650);
		panel.setLayout(null);
		panel.setVisible(true);
		add(panel);
		
		JLabel lblNewLabel = new JLabel("Pick Your Set");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 34));
		lblNewLabel.setBounds(298, 63, 204, 41);
		panel.add(lblNewLabel);
		
		JButton btnBack = new JButton("Back");
		btnBack.setBounds(60, 533, 89, 23);
		panel.add(btnBack);
		btnBack.addActionListener(eventHandler);
		
		list = new JList<String>(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setBounds(146, 179, 508, 267);
		panel.add(list);
		
		JButton btnPlay = new JButton("Play");
		btnPlay.setBounds(663, 533, 89, 23);
		panel.add(btnPlay);
		btnPlay.addActionListener(eventHandler);
	}
	public void refresh(){
		//.out.println("Debug menuListGen: " + setNames);
		//list.add(Sets.getSetNames().get(0), list); BROKEN
		for(int x=0;x<setNames.size();x++){
			if((listModel.getSize()==0)||(listModel.getSize()==x)||((listModel.getElementAt(x)).equals(setNames.get(x))==false)){
				listModel.addElement(setNames.get(x));
			}
		}
		
	}
	private class ButtonEventHandler implements ActionListener
    {

        public void actionPerformed(ActionEvent e)
        {
        	int listIndex = list.getSelectedIndex();
        	if(("Back").equals(e.getActionCommand())){
        		setVisible(false);
        		Menu.setPanelVis(true);
        	}
        	if(("Play").equals(e.getActionCommand())){
        		add(game);
        		game.loadSet(listIndex);
        		game.layButtons();
        		game.setVisible(true);
        		panel.setVisible(false);
        	}
        }
    }
	public void setPanelVis(boolean b){
		setVisible(b);
	}
	public static ArrayList<String> readSetList(){
		Scanner setfile = null;
		try {
			setfile = new Scanner(new File ("setlist.cfg"));
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
		ArrayList<String> setList = new ArrayList<String>();
		while(setfile.hasNextLine()){
			setList.add(setfile.nextLine());
		}
		setfile.close();
		//System.out.println("Debug setList: " + setList);
		return setList;
		
	}
	public static ArrayList<String> getSetNames(){
		//ArrayList<String> setList = readSetList();
		for(int x=0;x<setList.size();x++){
			if(((setList.get(x)).length()==0)||((setList.get(x)).substring(0,4).equals("Name")==false)){
				//System.out.println(setList.get(x));
				setList.remove(x);
				x=x-1;
			}
		}
		for(int x=0;x<setList.size();x++){
			setList.set(x,(setList.get(x)).substring(5));
		}
		return setList;
	}
	public static Integer setIndexLine(int setSelectedIndex){
		//ArrayList<String> setList = getSetList();
		ArrayList<Integer> setListIndex = new ArrayList<Integer>();
		//int debugX=0;
		for(int y=0;y<setList.size();y++){
			if((setList.get(y)).equals("[Set]")){
				setListIndex.add(y);
				//System.out.println(setListIndex.get(debugX));
				//debugX++;
			}
		}
		return setListIndex.get(setSelectedIndex);
	}
	public static ArrayList<String> getSetList() {
		return setList;
	}
	public static void setSetList(ArrayList<String> setList) {
		setList = SetSelect.setList;
	}
}
