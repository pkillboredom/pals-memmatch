package gui;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;



public class Menu extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 177214931369081867L;
	private ButtonEventHandler eventHandler = new ButtonEventHandler();
	private static JPanel menuPanel;
	private SetSelect setSelectFrame = new SetSelect();
	/**
	 * Create the panel.
	 */
	public Menu() {
		setBounds(0, 0, 800, 600);
		setVisible(true);
		setLayout(null);
		
		menuPanel = new JPanel();
		menuPanel.setBounds(0, 0, 800, 650);
		menuPanel.setLayout(null);
		menuPanel.setVisible(true);
		add(menuPanel);
		
		JLabel lblPalsMemoryMatch = new JLabel("PALS Memory Match Game");
		lblPalsMemoryMatch.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblPalsMemoryMatch.setBounds(239, 37, 321, 29);
		menuPanel.add(lblPalsMemoryMatch);
		
		JButton btnPlayGame = new JButton("Play Game");
		btnPlayGame.setBounds(341, 178, 117, 23);
		menuPanel.add(btnPlayGame);
		btnPlayGame.addActionListener(eventHandler);
		
		JButton btnAbout = new JButton("About");
		btnAbout.setBounds(701, 553, 89, 23);
		menuPanel.add(btnAbout);
		btnAbout.addActionListener(eventHandler);
		
		JButton btnQuit = new JButton("Quit");
		btnQuit.setBounds(10, 553, 89, 23);
		menuPanel.add(btnQuit);
		btnQuit.addActionListener(eventHandler);
	}
	private class ButtonEventHandler implements ActionListener
    {

        public void actionPerformed(ActionEvent e)
        {
        	//Debug for all button presses
        	//System.out.println(e.getActionCommand());
        	if("Quit".equals(e.getActionCommand())){
        		//System.out.println("Debug: Quit Pressed");
                System.exit(0);
        	}
        	if("About".equals(e.getActionCommand())){
                //System.out.println("Debug: About Pressed");
                About aboutFrame;
        		aboutFrame = new About();
                aboutFrame.setVisible(true);
        	}
        	if("Play Game".equals(e.getActionCommand())){
        		add(setSelectFrame);
        		setSelectFrame.refresh();
        		setSelectFrame.setVisible(true);
        		menuPanel.setVisible(false);
        	}
        }
    }
	public static void setPanelVis(boolean b){
		menuPanel.setVisible(b);
	}
}
