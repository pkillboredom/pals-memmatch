package gui;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import org.imgscalr.Scalr;

public class memButton extends JButton{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7419939887635235540L;
	private String item;
	private String filePath;
	private static int matches;
	private BufferedImage i;
	private String nameOfSet;
	private boolean clicked;
	
	public memButton(String item,String filePath,String nameOfSet){
		super("");
		this.item=item;
		//System.out.println(this.item);
		this.filePath=filePath;
		//System.out.println(this.filePath);
		this.nameOfSet=nameOfSet;
		i=Scalr.resize(getImage(),110);
		//System.out.println(this.nameOfSet);
		setBackground(new Color(255, 255, 255));
	}
	
	public boolean isMatch(Object o){
		memButton b =(memButton)o;
		if(item.equals(b.getItem())){
			return true;
		}
		return false;
	}
	
	public void setGot(){
		setEnabled(false);
		setBackground(new Color(0, 255, 0));
	}

	public static int getMatches() {
		return matches;
	}

	public static void setMatches() {
		memButton.matches = matches++;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	@Override
	public String toString() {
		return "memButton [filePath=" + filePath + ", item=" + item + "]";
	}
	
	private BufferedImage getImage(){
		String imgPath=nameOfSet+"/"+filePath;
		//System.out.println(item+" "+imgPath);
		try {
			return ImageIO.read(new File(imgPath));
		} catch (IOException e) {
			System.out.println("!!!No file for this Button!!!");
			//e.printStackTrace();
			try {
				return ImageIO.read(new File("img/placeholder.jpg"));
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(getParent(), "The placeholder image could not be loaded. Check to make sure " +
						"it is in /img/ or redownload the program.", "Critical Error", JOptionPane.ERROR_MESSAGE);
				System.exit(0);
			}
		}
		return null;
	}
	public void setClicked(boolean tf){
		if(tf==true){
			this.setBackground(new Color(0,0,255));
			//this.setEnabled(false);
			setIcon(new ImageIcon(i));
			clicked=true;
		}
		if(tf==false){
			this.setBackground(new Color(255,255,255));
			//this.setEnabled(true);
			setIcon(null);
			clicked=true;
		}
	}
	public boolean getClicked(){
		return clicked;
	}
	public void setMatched(){
		this.setBackground(new Color(0,255,0));
		this.setEnabled(false);
	}
}
