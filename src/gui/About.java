package gui;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;


@SuppressWarnings("serial")
public class About extends JFrame {
	private JPanel contentPane;
	public About(){
		setTitle("About");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(200, 150, 280, 518);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblText = new JLabel("");
		lblText.setIcon(new ImageIcon("img/logoabout.png"));
		contentPane.setLayout(null);
		lblText.setBounds(25, 7, 224, 199);
		contentPane.add(lblText);
		
		JLabel lblPalsg = new JLabel("<html>Copyright 2013 Panther Assisted Learning Software Group<br />" +
				"Licensed under the Apache License, Version 2.0 (the \"License\"); you may not use this file except in compliance" +
				" with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required" +
				" by applicable law or agreed to in writing, software distributed under the License is distributed on an \"AS IS\" BASIS," +
				" WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language" +
				" governing permissions and limitations under the License.</html>");
		lblPalsg.setBounds(10, 217, 254, 262);
		lblPalsg.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblPalsg);
		
		
	}
}
