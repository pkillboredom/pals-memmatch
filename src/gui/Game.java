package gui;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JPanel;

//import logic.Sets;


public class Game extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5946617636319572885L;
	private ArrayList<String> loadedSet = new ArrayList<String>();
	private memButton[][] buttonGrid = new memButton[5][4];
	private ButtonEventHandler eventHandler = new ButtonEventHandler();
	private static JPanel panel;
	private String setName;
	private memButton b1,b2;
	private int count=0;
	/**
	 * Create the panel.
	 */
	public Game() {
		setBounds(0, 0, 800, 600);
		setLayout(null);
		setVisible(true);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 650);
		panel.setLayout(null);
		panel.setVisible(true);
		add(panel);
	}
	public void loadSet(int setSelectedIndex){//Loads the set into the game.
		int line = SetSelect.setIndexLine(setSelectedIndex);
		for(int x = line;x<SetSelect.getSetList().size();x++){
			//System.out.println(SetSelect.setList.get(x));
			if(((SetSelect.getSetList().get(x)).length()>4)&&((SetSelect.getSetList().get(x)).substring(0,4).equals("Item")==true)){
				loadedSet.add(SetSelect.getSetList().get(x));
			}
		}
		getSetName(setSelectedIndex);
		System.out.println("Debug loadedSet: " + loadedSet);
	}
	public void getSetName(int setSelectedIndex){
		int line = SetSelect.setIndexLine(setSelectedIndex);
		for(int x = line;x<SetSelect.getSetList().size();x++){
			//System.out.println(SetSelect.setList.get(x));
			if(((SetSelect.getSetList().get(x)).length()>5)&&((SetSelect.getSetList().get(x)).substring(0,5).equals("Name=")==true)){
				setName=(SetSelect.getSetList().get(x)).substring(5);
				//System.out.println(setName);
				break;
			}
		}
	}
	public void buildButtons(){
		ArrayList<String> setToBuild = loadedSet;
		Random gen=new Random();
		for(int z=10;z>0;z--){
			int buttonItem=gen.nextInt(z); //Item to be buttonified
			int x=-1;//initializing variables so that we can catch for filled spots
			int y=-1;
			for(int a=0;a<2;a++){
				while(x==-1||buttonGrid[x][y]!=null){	//checks to make sure that we havent already put something in this coord
					x=gen.nextInt(5);
					y=gen.nextInt(4);
				}
				//System.out.println(setToBuild.get(buttonItem));
				String item=(setToBuild.get(buttonItem)).substring(5,(setToBuild.get(buttonItem)).indexOf(","));
				//System.out.println(item);
				String path=setToBuild.get(buttonItem).substring((setToBuild.get(buttonItem)).indexOf(",")+1);
				//System.out.println(path);
				buttonGrid[x][y]=new memButton(item,path,setName);
				//System.out.println(buttonGrid[x][y]);
			}
			setToBuild.remove(buttonItem);//makes sure that we dont use an item twice
		}
	}
	public void layButtons(){
		buildButtons();
		buttonGrid[0][0].setBounds(10, 10, 140, 120);
		panel.add(buttonGrid[0][0]);
		buttonGrid[0][0].addActionListener(eventHandler);
		
		buttonGrid[1][0].setBounds(168, 10, 140, 120);
		panel.add(buttonGrid[1][0]);
		buttonGrid[1][0].addActionListener(eventHandler);
		
		buttonGrid[2][0].setBounds(326, 10, 140, 120);
		panel.add(buttonGrid[2][0]);
		buttonGrid[2][0].addActionListener(eventHandler);
		
		buttonGrid[3][0].setBounds(487, 10, 140, 120);
		panel.add(buttonGrid[3][0]);
		buttonGrid[3][0].addActionListener(eventHandler);
		
		buttonGrid[4][0].setBounds(650, 10, 140, 120);
		panel.add(buttonGrid[4][0]);
		buttonGrid[4][0].addActionListener(eventHandler);
		
		buttonGrid[0][1].setBounds(10, 155, 140, 120);
		panel.add(buttonGrid[0][1]);
		buttonGrid[0][1].addActionListener(eventHandler);
		
		buttonGrid[1][1].setBounds(168, 155, 140, 120);
		panel.add(buttonGrid[1][1]);
		buttonGrid[1][1].addActionListener(eventHandler);
		
		buttonGrid[2][1].setBounds(326, 155, 140, 120);
		panel.add(buttonGrid[2][1]);
		buttonGrid[2][1].addActionListener(eventHandler);
		
		buttonGrid[3][1].setBounds(487, 155, 140, 120);
		panel.add(buttonGrid[3][1]);
		buttonGrid[3][1].addActionListener(eventHandler);
		
		buttonGrid[4][1].setBounds(650, 155, 140, 120);
		panel.add(buttonGrid[4][1]);
		buttonGrid[4][1].addActionListener(eventHandler);
		
		buttonGrid[0][2].setBounds(10, 300, 140, 120);
		panel.add(buttonGrid[0][2]);
		buttonGrid[0][2].addActionListener(eventHandler);
		
		buttonGrid[1][2].setBounds(168, 300, 140, 120);
		panel.add(buttonGrid[1][2]);
		buttonGrid[1][2].addActionListener(eventHandler);
		
		buttonGrid[2][2].setBounds(326, 300, 140, 120);
		panel.add(buttonGrid[2][2]);
		buttonGrid[2][2].addActionListener(eventHandler);
		
		buttonGrid[3][2].setBounds(487, 300, 140, 120);
		panel.add(buttonGrid[3][2]);
		buttonGrid[3][2].addActionListener(eventHandler);
		
		buttonGrid[4][2].setBounds(650, 300, 140, 120);
		panel.add(buttonGrid[4][2]);
		buttonGrid[4][2].addActionListener(eventHandler);
		
		buttonGrid[0][3].setBounds(10, 445, 140, 120);
		panel.add(buttonGrid[0][3]);
		buttonGrid[0][3].addActionListener(eventHandler);
		
		buttonGrid[1][3].setBounds(168, 445, 140, 120);
		panel.add(buttonGrid[1][3]);
		buttonGrid[1][3].addActionListener(eventHandler);
		
		buttonGrid[2][3].setBounds(326, 445, 140, 120);
		panel.add(buttonGrid[2][3]);
		buttonGrid[2][3].addActionListener(eventHandler);
		
		buttonGrid[3][3].setBounds(487, 445, 140, 120);
		panel.add(buttonGrid[3][3]);
		buttonGrid[3][3].addActionListener(eventHandler);
		
		buttonGrid[4][3].setBounds(650, 445, 140, 120);
		panel.add(buttonGrid[4][3]);
		buttonGrid[4][3].addActionListener(eventHandler);
	}
	private class ButtonEventHandler implements ActionListener
    {

        public void actionPerformed(ActionEvent e)
        {
        	System.out.println(e.getSource());
        	if(e.getSource() instanceof memButton){
        		if(count==0){
        			b1=(memButton)e.getSource();
        			b1.setClicked(true);
        			count++;
        		}
        		else if(count==1&&(memButton)e.getSource()!=b1){
        			b2=(memButton)e.getSource();
        			System.out.println(b2);
        			count=0;
        			if(b1.isMatch(b2)){
        				b1.setMatched();
        				b2.setClicked(true);
        				b2.setMatched();
        				memButton.setMatches();
        			}
        			else{
        				//System.out.println(b2);
        				b2.setClicked(true);
        				if(b1.getClicked()==true&&b2.getClicked()==true){
        					/*
        					try {
        						Thread.sleep(1000);
        					} catch (InterruptedException e1) {
        						// TODO Auto-generated catch block
        						e1.printStackTrace();
        					}
        					*/
        				}

						
        				//b2.setClicked(false);
        				//b1.setClicked(false);
        				count=0;
        			}
        			if(memButton.getMatches()==10){
        				System.out.println("Game Done!");
        				//TODO Implement actual ending later
        			}
        		}
        	}
        }
    }
}
